﻿namespace Assignment28;

public static class Extensions
{
	public static long ForeachSum(this IEnumerable<long> enumerable)
	{
		long result = 0;
		foreach (var item in enumerable)
		{
			result += item;
		}
		return result;
	}
	
	public static async Task<long> ParallelSum(this IEnumerable<long> enumerable)
	{
		var parts = enumerable.Split();

		var tasks = parts
			.Select(part => Task.Factory.StartNew(part.ForeachSum));

		var results = await Task.WhenAll(tasks);
		long result = 0;
		foreach (var item in results)
		{
			result += item;
		}

		return result;
	}

	private static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> list)
	{
		var count = 2;

		var bunchLength = list.Count() / count;
		for (int i = 0; i <= count; i++)
		{
			yield return list.Skip(i * bunchLength).Take(bunchLength);
		}
	}
}