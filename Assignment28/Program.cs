﻿using System.Collections;
using System.Diagnostics;
using System.Linq;

namespace Assignment28
{
	internal class Program
	{
		static async Task Main(string[] args)
		{
			var intList100k = Enumerable.Range(0, 100000).Select(i => (long)i).ToList();
			var intList1m = Enumerable.Range(0, 1000000).Select(i => (long)i).ToList();
			var intList10m = Enumerable.Range(0, 10000000).Select(i => (long)i).ToList();

			var stopwatch = new Stopwatch();

			Console.WriteLine("Regular sum");
			stopwatch.Start();
			intList100k.ForeachSum();
			Console.WriteLine($"10k {stopwatch.Elapsed}");

			stopwatch.Restart();
			intList1m.ForeachSum();
			Console.WriteLine($"1m  {stopwatch.Elapsed}");

			stopwatch.Restart();
			intList10m.ForeachSum();
			Console.WriteLine($"10m {stopwatch.Elapsed}");
			stopwatch.Reset();


			Console.WriteLine("Parallel sum");
			stopwatch.Start();
			await intList100k.ParallelSum();
			Console.WriteLine($"10k {stopwatch.Elapsed}");

			stopwatch.Restart();
			await intList1m.ParallelSum();
			Console.WriteLine($"1m  {stopwatch.Elapsed}");

			stopwatch.Restart();
			await intList10m.ParallelSum();
			Console.WriteLine($"10m {stopwatch.Elapsed}");
			stopwatch.Reset();


			Console.WriteLine("LINQ sum");
			stopwatch.Start();
			intList100k.AsParallel().Sum();
			Console.WriteLine($"10k {stopwatch.Elapsed}");

			stopwatch.Restart();
			intList1m.AsParallel().Sum();
			Console.WriteLine($"1m  {stopwatch.Elapsed}");

			stopwatch.Restart();
			intList10m.AsParallel().Sum();
			Console.WriteLine($"10m {stopwatch.Elapsed}");
			stopwatch.Reset();


			Console.ReadLine();
		}
	}
}